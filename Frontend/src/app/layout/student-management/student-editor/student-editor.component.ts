import {Component, NgZone, OnInit, ViewChild} from '@angular/core';
import {StudentManagementService} from '../../../services/student-management.service';
import {Student} from '../../../services/models/student';
import {ActivatedRoute, Router} from '@angular/router';
import {DialogService} from '../../../shared/services/dialog.services';

@Component({
    selector: 'app-student-editor',
    templateUrl: './student-editor.component.html',
    styleUrls: ['./student-editor.component.scss'],
    providers: [StudentManagementService],

})
export class StudentEditorComponent implements OnInit {


    private student: Student;
    private busy: boolean;

    constructor(private zone: NgZone, private studentService: StudentManagementService, private route: ActivatedRoute, private dialogService: DialogService, private router: Router) {

    }

    ngOnInit() {

        if (this.route.params['id'] == null) {
            this.student = new Student();
        }
        this.route.params.subscribe(params => {
            const idParam = params['id'];
            if (idParam != null) {
                this.busy = true;
                this.studentService.getStudent(idParam).then(student => {

                    this.student = student;
                    this.busy = false;
                });
            }
        });
    }

    private  onSubmit() {
        this.busy = true;
        this.studentService.saveStudent(this.student).then(id => {
            this.busy = false;
            this.navigateToStudentList(id);
        });
    }

    private confirmCancel(pristine: boolean): boolean {
        if (!pristine) {
            this.dialogService.confirm().then(cancelConfirmed => {
                if (cancelConfirmed === true) {
                    this.navigateToStudentList();
                }
            });
        } else {
            this.navigateToStudentList();
        }
        return false;
    }

    private navigateToStudentList(id: number = 0) {
        const param = id === 0 ? '' : `${id}`;
        this.router.navigateByUrl(`/student-management/${param}`);
    }
}
