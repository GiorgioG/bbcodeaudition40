import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {StudentListComponent} from './student-list.component';
import {StudentEditorComponent} from "../student-editor/student-editor.component";

const routes: Routes = [
    { path: '', component: StudentListComponent },
    { path: 'edit', component: StudentEditorComponent },
    { path: 'edit/:id', component: StudentEditorComponent },
    { path: ':id', component: StudentListComponent },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class StudentManagementRoutingModule { }
