import {Component, NgZone, OnInit} from '@angular/core';
import {StudentManagementService} from '../../../services/student-management.service';
import {Student} from '../../../services/models/student';
import {ActivatedRoute} from '@angular/router';
import * as _ from 'lodash';
import {DialogService} from '../../../shared/services/dialog.services';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.scss'],
    providers: [StudentManagementService]
})
export class StudentListComponent implements OnInit {
    private busy: boolean;

    students: Student[];
    constructor(private zone: NgZone, private studentService: StudentManagementService, private route: ActivatedRoute, private dialogService: DialogService) {

    }

  ngOnInit() {
        this.busy = true;
        this.studentService.getStudents().then(results => {
            this.students = results;
            this.busy = false;
            this.route.params.subscribe(params => {
                const idParam = params['id'];
                if (idParam != null) {
                    const id = Number(idParam);
                    const student = _.find(this.students, x => x.id === id);
                    if (student != null) {
                        student.updated = true;
                    }
                }
            });
        });
  }

    confirmDeleteStudent(student: Student, el: any) {
        this.dialogService.confirm('Danger!', `Are you sure you want to delete '${student.getFullName()}'?`).then(deleteStudentConfirmed => {
            if (deleteStudentConfirmed === true) {
                this.studentService.deleteStudent(student.id).then(result => {
                    _.remove(this.students, x => x.id === student.id);
                });
            }
        });
    }

}
