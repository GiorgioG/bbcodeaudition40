import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentListComponent } from './student-list/student-list.component';
import { StudentEditorComponent } from './student-editor/student-editor.component';
import {StudentManagementRoutingModule} from './student-list/student-management-routing.module';
import {PageHeaderModule} from '../../shared/modules/page-header/page-header.module';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule} from '@angular/forms';
import {NgbDateParserFormatter, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {DateValidator} from '../../shared/directives/DateValidator';
import {DialogService} from '../../shared/services/dialog.services';
import {DialogComponent} from '../../shared/components/dialog/dialog.component';

@NgModule({
  imports: [
    CommonModule,
      StudentManagementRoutingModule,
      PageHeaderModule,
      TranslateModule,
      FormsModule,
      NgbModule.forRoot()
  ],
  declarations: [StudentListComponent, StudentEditorComponent, DateValidator, DialogComponent],
    providers: [DialogService],
    entryComponents: [DialogComponent]

})
export class StudentManagementModule { }
