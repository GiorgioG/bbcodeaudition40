import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-dashboard',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public alerts: Array<any> = [];
    constructor() {


        this.alerts.push({
            id: 1,
            type: 'success',
            message: 'Click on \'Students\' in the sidebar to get started!',
        });
    }
    ngOnInit() {}

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
