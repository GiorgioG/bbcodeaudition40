export function GetFormattedDate(date: Date) {
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    return `${month}/${day}/${year}`;
}
