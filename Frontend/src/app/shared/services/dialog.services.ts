import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DialogComponent} from '../components/dialog/dialog.component';
import {Injectable} from '@angular/core';


@Injectable()
export class DialogService {
    constructor(private modalService: NgbModal) {}

    public confirm(title: string = 'Discard Changes?', message:string = 'Are you sure you want to discard your changes?') {
        const modalRef = this.modalService.open(DialogComponent);
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.message = message;
        modalRef.componentInstance.changeRef.markForCheck();
        return modalRef.result;
    }
}
