import {debug} from "util";
export class Student {
    public deleted: boolean; // These two are used only for the student list view
    public updated: boolean;


    constructor(public id: number = 0,
                public firstName: string = null,
                public lastName: string = null,
                public grade: string = null,
                public dateOfBirth: string = null) {

    }

    public getFullName(): string {
        return `${this.lastName}, ${this.firstName}`;
    }

    public isNew(): boolean {
        return this.id === 0;
    }
}
