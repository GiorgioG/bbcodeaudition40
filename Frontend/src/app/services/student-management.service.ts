import {Injectable, NgZone} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {Student} from './models/student';
import {environment} from '../../environments/environment';
import {GetFormattedDate} from '../shared/utils/DateHelper';

@Injectable()
export class StudentManagementService {

    private readonly base_url = `/api/student`;

    constructor(private http: Http) {
    }

    getStudent(id: number): Promise<Student> {

        return this.http.get(`${this.base_url}/${id}`)
            .toPromise()
            .then(response => this.mapToStudent(response.json()))
            .catch(this.handleError);
    }

    getStudents(): Promise<Student[]> {

        return this.http.get(this.base_url)
            .toPromise()
            .then(response => this.mapToStudents(response.json()))
            .catch(this.handleError);
    }

    private  mapToStudents(data: [any]): Student[] {
        const students = [];
        for (const studentItem of data) {
            students.push(this.mapToStudent(studentItem));
        }
        return students;
    }

    private mapToStudent(dataElement: any) {
        if (dataElement == null) return null;
        return new Student(dataElement.id, dataElement.firstName, dataElement.lastName, dataElement.grade, GetFormattedDate(new Date(dataElement.dateOfBirth)));
    }


    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    saveStudent(student: Student): Promise<any> {
        if (student.isNew()) {
            return this.http.post(this.base_url, student)
                .toPromise()
                .then(result => result.json().id)
                .catch(this.handleError);
        } else {
            return this.http.put(`${this.base_url}/${student.id}`, student)
                .toPromise()
                .then(result => student.id)
                .catch(this.handleError);
        }
    }

    deleteStudent(id: number) {
        return this.http.delete(`${this.base_url}/${id}`)
            .toPromise()
            .catch(this.handleError);
    }
}
