# Mission:

## We are building a Student Information System for an international online school.
## We would like you to create an Angular app supported by a C# WebAPI that will facilitate CRUD operations on student records

* A student record will consist of the following:
    
    - First and Last Name (Up to 100 Characters each)
    - Grade (Up to 2 characters. This represents the current grade the student is in)
    - Date of Birth


* All records in the system have timestamps indicating the date the record was created and last updated.

* Please note that we never hard delete records.

* Please structure the application using enterprise level architecture / patterns.

* Put all work in a branch named `feature/smart-code-audition`

* An Azure SQL server instance is available at `smarttester.database.windows.net` for database work (same username/password)


When you have completed the mission, we will meet for a demo. You will have a chance to showcase your work (both output and design)