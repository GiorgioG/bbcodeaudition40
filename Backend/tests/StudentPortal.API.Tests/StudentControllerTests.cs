using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using StudentPortal.API.Controllers;
using StudentPortal.Core.Contract.Domain.Students;
using Xunit;
using Moq;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Presentation.Converters;
using StudentPortal.Core.Presentation.Models;
using StudentPortal.Core.Shared.Errors;

namespace StudentPortal.API.Tests
{
    public sealed class StudentControllerTests
    {
        [Fact]
        public void StudentController_Get_Success()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.GetStudents(It.IsAny<int>(), It.IsAny<int>())).Returns(() => new List<StudentEntity> {ProduceStudentEntity()});
            var result = controller.Get();

            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);
            var okResult = result as OkObjectResult;

            Assert.NotNull(okResult.Value);
            Assert.True(((IEnumerable<StudentPresentationModel>)okResult.Value).Any());
        }

        [Fact]
        public void StudentController_GetById_Failure_Invalid_Id()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;

            var result = controller.Get(0);

            Assert.NotNull(result);
            Assert.IsType<BadRequestObjectResult>(result);
            var badRequest = result as BadRequestObjectResult;

            Assert.Equal(400, badRequest.StatusCode);

            Assert.NotNull(badRequest.Value);
            Assert.Equal("Invalid id", badRequest.Value);
        }

        [Fact]
        public void StudentController_GetById_Failure_Id_Does_Not_Exist()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.Exists(It.IsAny<int>())).Returns(() => false);

            var result = controller.Get(int.MaxValue);

            Assert.NotNull(result);
            Assert.IsType<NotFoundResult>(result);
            var notFoundResult = result as NotFoundResult;

            Assert.Equal(404, notFoundResult.StatusCode);
        }

        [Fact]
        public void StudentController_GetById_Failure_Unknown_Error()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.Exists(It.IsAny<int>())).Returns(() => true);
            studentServiceMock.Setup(service => service.GetStudentById(It.IsAny<int>())).Returns(() => (null, new Error("Unknown Error", ErrorIds.Data.UnknownError)));

            var result = controller.Get(int.MaxValue);

            Assert.NotNull(result);
            AssertBadRequest(result, "Unknown Error");
        }

        [Fact]
        public void StudentController_GetById_Success()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.Exists(It.IsAny<int>())).Returns(() => true);
            studentServiceMock.Setup(service => service.GetStudentById(It.IsAny<int>())).Returns(() => (ProduceStudentEntity(),null));

            var result = controller.Get(int.MaxValue);

            Assert.NotNull(result);
            Assert.IsType<OkObjectResult>(result);
            var okResult = result as OkObjectResult;

            Assert.NotNull(okResult.Value);
            Assert.IsType<StudentPresentationModel>(okResult.Value);
            Assert.Equal(200, okResult.StatusCode);
        }

        [Fact]
        public void StudentController_Delete_Failure_Invalid_Id()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;

            var result = controller.Delete(0);

            Assert.NotNull(result);
            Assert.IsType<BadRequestObjectResult>(result);
            var badRequest = result as BadRequestObjectResult;

            Assert.Equal(400, badRequest.StatusCode);

            Assert.NotNull(badRequest.Value);
            Assert.Equal("Invalid id", badRequest.Value);
        }

        [Fact]
        public void StudentController_Delete_Failure_Id_Does_Not_Exist()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.Exists(It.IsAny<int>())).Returns(() => false);

            var result = controller.Delete(int.MaxValue);

            Assert.NotNull(result);
            Assert.IsType<NotFoundResult>(result);
            var notFoundResult = result as NotFoundResult;

            Assert.Equal(404, notFoundResult.StatusCode);
        }

        [Fact]
        public void StudentController_Delete_Success()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.Exists(It.IsAny<int>())).Returns(() => true);
            studentServiceMock.Setup(service => service.DeleteStudent(It.IsAny<int>())).Returns(() => null);

            var result = controller.Delete(int.MaxValue);

            Assert.NotNull(result);
            Assert.IsType<AcceptedResult>(result);
            var okResult = result as AcceptedResult;

            Assert.Equal(202, okResult.StatusCode);
        }

        [Fact]
        public void StudentController_Delete_Failure_Unknown_Error()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.Exists(It.IsAny<int>())).Returns(() => true);
            studentServiceMock.Setup(service => service.DeleteStudent(It.IsAny<int>())).Returns(() => new Error("Unknown Error", ErrorIds.Data.UnknownError));

            var result = controller.Delete(int.MaxValue);

            Assert.NotNull(result);
            AssertBadRequest(result, "Unknown Error");
        }

        [Fact]
        public void StudentController_Put_Failure_Invalid_Id()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;

            var result = controller.Put(0,null);

            Assert.NotNull(result);
            Assert.IsType<BadRequestObjectResult>(result);
            var badRequest = result as BadRequestObjectResult;

            Assert.Equal(400, badRequest.StatusCode);

            Assert.NotNull(badRequest.Value);
            Assert.Equal("Invalid id", badRequest.Value);
        }

        [Fact]
        public void StudentController_Put_Failure_Id_Does_Not_Exist()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.Exists(It.IsAny<int>())).Returns(() => false);

            var result = controller.Put(1, ProduceStudentPresentationModel(1));

            Assert.NotNull(result);
            Assert.IsType<NotFoundResult>(result);
            var notFoundResult = result as NotFoundResult;

            Assert.Equal(404, notFoundResult.StatusCode);
        }

        [Fact]
        public void StudentController_Put_Failure_Id_Mismatch()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;

            var result = controller.Put(1, ProduceStudentPresentationModel(2));

            Assert.NotNull(result);
            Assert.IsType<BadRequestObjectResult>(result);
            var badRequestResult = result as BadRequestObjectResult;

            var errorMessage = badRequestResult.Value as string;
            Assert.NotNull(errorMessage);
            Assert.Equal("Uri/model id mismatch.", errorMessage);

            Assert.Equal(400, badRequestResult.StatusCode);
        }

        [Fact]
        public void StudentController_Put_Success()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.Exists(It.IsAny<int>())).Returns(() => true);
            studentServiceMock.Setup(service => service.UpdateStudent(It.IsAny<StudentEntity>())).Returns(() => (ProduceStudentEntity(),null));

            var result = controller.Put(1,ProduceStudentPresentationModel(1));

            Assert.NotNull(result);
            Assert.IsType<AcceptedResult>(result);
            var okResult = result as AcceptedResult;

            Assert.Equal(202, okResult.StatusCode);
        }

        [Fact]
        public void StudentController_Put_Failure_Unknown_Error()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;
            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.Exists(It.IsAny<int>())).Returns(() => true);
            studentServiceMock.Setup(service => service.UpdateStudent(It.IsAny<StudentEntity>())).Returns(() => (null, new Error("Unknown Error", ErrorIds.Data.UnknownError)));

            var result = controller.Put(1, ProduceStudentPresentationModel(1));

            Assert.NotNull(result);
            AssertBadRequest(result, "Unknown Error");

        }

        private static void AssertBadRequest(IActionResult result, string errorMessage)
        {

            Assert.IsType<BadRequestObjectResult>(result);

            var badRequestResult = result as BadRequestObjectResult;

            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.NotNull(badRequestResult.Value);
            Assert.IsType<Error>(badRequestResult.Value);

            var error = badRequestResult.Value as Error;

            Assert.Equal(errorMessage, error.Message);
            Assert.Equal(ErrorIds.Data.UnknownError, error.ErrorId);
        }


        [Fact]
        public void StudentController_Put_Failure_Null_Payload()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;

            var result = controller.Put(1, null);

            Assert.NotNull(result);
            Assert.IsType<BadRequestObjectResult>(result);

            var badRequestResult = result as BadRequestObjectResult;

            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.NotNull(badRequestResult.Value);
            Assert.IsType<string>(badRequestResult.Value);

            var errorMessage = badRequestResult.Value as string;

            Assert.Equal("Invalid/missing request body.", errorMessage);
        }


        [Fact]
        public void StudentController_Post_Failure_Null_Payload()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;

            var result = controller.Post(null);

            Assert.NotNull(result);
            Assert.IsType<BadRequestObjectResult>(result);

            var badRequestResult = result as BadRequestObjectResult;

            Assert.Equal(400, badRequestResult.StatusCode);
            Assert.NotNull(badRequestResult.Value);
            Assert.IsType<string>(badRequestResult.Value);

            var errorMessage = badRequestResult.Value as string;

            Assert.Equal("Invalid/missing request body.", errorMessage);
        }

        [Fact]
        public void StudentController_Post_Success()
        {
            var setup = ProduceStudentController();
            var controller = setup.controller;

            var studentServiceMock = setup.studentServiceMock;

            studentServiceMock.Setup(service => service.CreateStudent(It.IsAny<StudentEntity>())).Returns(() => (ProduceStudentEntity(), null));

            var result = controller.Post(ProduceStudentPresentationModel());

            Assert.NotNull(result);
            Assert.IsType<CreatedResult>(result);
            var okResult = result as CreatedResult;

            Assert.NotNull(okResult.Value);
            Assert.IsType<StudentPresentationModel>(okResult.Value);
            Assert.Equal(201, okResult.StatusCode);
        }

        private static (StudentController controller, Mock<IStudentService> studentServiceMock) ProduceStudentController()
        {
            var studentServiceMock = new Mock<IStudentService>();

            var studentController = new StudentController(studentServiceMock.Object, new StudentConverter());

            var urlHelper = new Mock<IUrlHelper>();
            urlHelper.Setup(x => x.Content(It.IsAny<string>())).Returns(()=> string.Empty);
            studentController.Url = urlHelper.Object;

            return (studentController, studentServiceMock);
        }

        private static StudentEntity ProduceStudentEntity()
        {
            return new StudentEntity()
            {
                FirstName = "Gabby",
                LastName = "Galante",
                DateOfBirth = DateTime.UtcNow.AddYears(10),
                Grade = "6"
            };
        }

        private static StudentPresentationModel ProduceStudentPresentationModel(int id=0)
        {
            return new StudentPresentationModel()
            {
                Id=id,
                FirstName = "Gabby",
                LastName = "Galante",
                DateOfBirth = DateTime.UtcNow.AddYears(10),
                Grade = "6"
            };
        }
    }
}
