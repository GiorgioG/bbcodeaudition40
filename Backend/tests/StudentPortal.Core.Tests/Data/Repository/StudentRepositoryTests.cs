using System;
using System.Linq;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Data.Repository;
using StudentPortal.Core.Shared.Errors;
using Xunit;

namespace StudentPortal.Core.Tests.Data.Repository
{
    public sealed class StudentRepositoryIntegrationTests
    {
        private StudentEntity ProduceStudent()
        {
            return new StudentEntity
            {
                FirstName = "Giorgio",
                LastName = "Galante",
                DateOfBirth = new DateTime(1981, 12, 14),
                Grade = "02"
            };
        }

        private static (StudentEntity studentEntity, Error error) CreateStudent(StudentEntity studentEntity)
        {
            var studentRepository = ProduceStudentRepository();
            return studentRepository.CreateStudent(studentEntity);
        }

        private static StudentRepository ProduceStudentRepository()
        {
            const string connectionString = "Server=tcp:smarttester.database.windows.net,1433;Database=smarttestersql;User ID=bbcodeaudition40;Password=smartcodeaudition$3074;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";

            var studentRepository = new StudentRepository(new RepositoryConfiguration(connectionString));
            return studentRepository;
        }

        private static void AssertFieldsMatch(StudentEntity input, StudentEntity output)
        {
            Assert.Equal(input.FirstName, output.FirstName);
            Assert.Equal(input.LastName, output.LastName);
            Assert.Equal(input.DateOfBirth, output.DateOfBirth);
            Assert.Equal(input.Grade, output.Grade);
        }

        [Fact]
        public void StudentRepository_Create()
        {
            var studentEntity = ProduceStudent();

            var result = CreateStudent(studentEntity);

            Assert.NotNull(result);
            Assert.NotNull(result.studentEntity);
            Assert.Null(result.error);

            AssertFieldsMatch(studentEntity, result.studentEntity);
            Assert.NotEqual(0, result.studentEntity.Id);
            Assert.NotEqual(studentEntity.DateCreated, result.studentEntity.DateCreated);
        }

        [Fact]
        public void StudentRepository_Delete_Failure()
        {
            var studentRepository = ProduceStudentRepository();
            var error = studentRepository.DeleteStudent(int.MaxValue);
            Assert.NotNull(error);
            Assert.Equal(error.ErrorId, ErrorIds.Data.NotFound);
        }

        [Fact]
        public void StudentRepository_Delete_Success()
        {
            var studentEntity = ProduceStudent();

            var result = CreateStudent(studentEntity);
            studentEntity = result.studentEntity;

            var studentRepository = ProduceStudentRepository();
            var error = studentRepository.DeleteStudent(studentEntity.Id);
            Assert.Null(error);
        }

        [Fact]
        public void StudentRepository_Exists_False()
        {
            var studentRepository = ProduceStudentRepository();
            var exists = studentRepository.Exists(int.MaxValue);

            Assert.False(exists);
        }

        [Fact]
        public void StudentRepository_Exists_True()
        {
            var studentEntity = ProduceStudent();

            var result = CreateStudent(studentEntity);
            studentEntity = result.studentEntity;

            var studentRepository = ProduceStudentRepository();
            var exists = studentRepository.Exists(studentEntity.Id);

            Assert.True(exists);
        }

        [Fact]
        public void StudentRepository_GetById_Failure()
        {
            var studentRepository = ProduceStudentRepository();
            var result = studentRepository.GetStudentById(int.MaxValue);

            Assert.NotNull(result);
            Assert.Null(result.student);
            Assert.NotNull(result.error);
            Assert.Equal(ErrorIds.Data.NotFound, result.error.ErrorId);
        }

        [Fact]
        public void StudentRepository_GetById_Success()
        {
            var studentEntity = ProduceStudent();

            var result = CreateStudent(studentEntity);
            studentEntity = result.studentEntity;

            var studentRepository = ProduceStudentRepository();
            result = studentRepository.GetStudentById(studentEntity.Id);

            Assert.NotNull(result);
            Assert.NotNull(result.studentEntity);
            Assert.Null(result.error);
            AssertFieldsMatch(studentEntity, result.studentEntity);
        }

        [Fact]
        public void StudentRepository_GetStudents()
        {
            var studentEntity = ProduceStudent();

            // Make sure there's at least one student
            CreateStudent(studentEntity);

            var studentRepository = ProduceStudentRepository();
            var students = studentRepository.GetStudents();

            Assert.NotNull(students);
            Assert.True(students.Any());
        }

        [Fact]
        public void StudentRepository_Initialization_Null_Configuration()
        {
            Assert.Throws<ArgumentNullException>(() => new StudentRepository(null));
        }

        [Fact]
        public void StudentRepository_Initialization_Success()
        {
            const string connectionString = "Server=.;Database=master;Trusted_Connection=True";

            var configuration = new RepositoryConfiguration(connectionString);
            var studentRepository = new StudentRepository(configuration);

            Assert.NotNull(studentRepository);
        }

        [Fact]
        public void StudentRepository_Update_Failure_NotFound()
        {
            var studentRepository = ProduceStudentRepository();

            var result = studentRepository.UpdateStudent(new StudentEntity {Id = int.MaxValue});
            Assert.NotNull(result);
            Assert.Null(result.student);
            Assert.Equal(ErrorIds.Data.NotFound, result.error.ErrorId);
        }

        [Fact]
        public void StudentRepository_Update_Success()
        {
            var studentEntity = ProduceStudent();

            var result = CreateStudent(studentEntity);
            studentEntity = result.studentEntity;

            const string postfix = " (Updated)";

            studentEntity.FirstName = studentEntity.FirstName + postfix;
            studentEntity.LastName = studentEntity.LastName + postfix;
            studentEntity.Grade = "03";
            studentEntity.DateOfBirth = studentEntity.DateOfBirth.AddYears(-1);

            var studentRepository = ProduceStudentRepository();
            result = studentRepository.UpdateStudent(studentEntity);

            Assert.NotNull(result);
            AssertFieldsMatch(studentEntity, result.studentEntity);
            Assert.NotNull(result.studentEntity.DateLastModified);
        }
    }
}