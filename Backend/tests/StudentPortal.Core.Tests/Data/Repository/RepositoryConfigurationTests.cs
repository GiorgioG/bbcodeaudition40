﻿using System;
using StudentPortal.Core.Data.Repository;
using Xunit;

namespace StudentPortal.Core.Tests.Data.Repository
{
    public sealed class RepositoryConfigurationTests
    {
        [Fact]
        public void RepositoryConfiguration_Null_ConnectionString()
        {
            Assert.Throws<ArgumentOutOfRangeException>(()=>new RepositoryConfiguration(null));
        }

        [Fact]
        public void RepositoryConfiguration_Empty_ConnectionString()
        {
            Assert.Throws<ArgumentOutOfRangeException>(()=>new RepositoryConfiguration(string.Empty));
        }
        [Fact]
        public void RepositoryConfiguration_Ensure_ConnectionString_Matches_Input()
        {
            const string connectionString = "asdfjkl;";
            var configuration = new RepositoryConfiguration(connectionString);

            Assert.Equal(connectionString, configuration.ConnectionString);
        }
    }
}