﻿using System;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Presentation.Converters;
using StudentPortal.Core.Presentation.Models;
using Xunit;

namespace StudentPortal.Core.Tests.Presentation.Converters
{
    public sealed class StudentConverterTests
    {
        [Fact]
        public void StudentConverter_ToPresentationModel_Ensure_Mappings()
        {
            var studentEntity = new StudentEntity()
            {
                Id = 1,
                FirstName = "Giorgio",
                LastName = "Galante",
                Grade = "02",
                DateOfBirth = new DateTime(1981, 12, 14),
                DateCreated = DateTime.UtcNow,
                DateLastModified = DateTime.UtcNow
            };

            var studentConverter = new StudentConverter();

            var studentPM = studentConverter.ToPresentationModel(studentEntity);

            Assert.NotNull(studentPM);
            Assert.Equal(studentEntity.Id, studentPM.Id);
            Assert.Equal(studentEntity.FirstName, studentPM.FirstName);
            Assert.Equal(studentEntity.LastName, studentPM.LastName);
            Assert.Equal(studentEntity.Grade, studentPM.Grade);
            Assert.Equal(studentEntity.DateCreated, studentPM.DateCreated);
            Assert.Equal(studentEntity.DateLastModified, studentPM.DateLastModified);

        }

        [Fact]
        public void StudentConverter_ToEntity_Ensure_Mappings()
        {
            var studentPM = new StudentPresentationModel()
            {
                Id = 1,
                FirstName = "Giorgio",
                LastName = "Galante",
                Grade = "02",
                DateOfBirth = new DateTime(1981, 12, 14),
                DateCreated = DateTime.UtcNow,
                DateLastModified = DateTime.UtcNow
            };

            var studentConverter = new StudentConverter();
            var entity = studentConverter.ToEntity(studentPM);

            Assert.NotNull(entity);
            Assert.Equal(studentPM.Id, entity.Id);
            Assert.Equal(studentPM.FirstName, entity.FirstName);
            Assert.Equal(studentPM.LastName, entity.LastName);
            Assert.Equal(studentPM.Grade, entity.Grade);
            Assert.Equal(studentPM.DateCreated, entity.DateCreated);
            Assert.Equal(studentPM.DateLastModified, entity.DateLastModified);

        }

        [Fact]
        public void StudentConverter_ToPresentationModel_Null_Argument()
        {
            var studentConverter = new StudentConverter();
            var result = studentConverter.ToPresentationModel(null);
            Assert.Null(result);
        }
        [Fact]
        public void StudentConverter_ToEntity_Null_Argument()
        {
            var studentConverter = new StudentConverter();
            var result = studentConverter.ToEntity(null);
            Assert.Null(result);
        }
    }
}