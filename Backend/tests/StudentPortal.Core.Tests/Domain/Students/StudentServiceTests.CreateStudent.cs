﻿using System;
using System.Linq;
using Moq;
using StudentPortal.Core.Contract.Data.Repository;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Shared.Errors;
using Xunit;

namespace StudentPortal.Core.Tests.Domain.Students
{
    /// <summary>
    /// Class StudentService.
    /// </summary>
    public sealed partial class StudentServiceTests
    {
        [Fact]
        public void StudentService_CreateStudent_Null_Student()
        {
            var studentRepo = new Mock<IStudentRepository>();
            var service = ProduceStudentService(studentRepo);
            Assert.Throws<ArgumentNullException>(()=>service.CreateStudent(null));
        }

        [Fact]
        public void StudentService_CreateStudent_Invalid_Student_Multiple_Validation_Errors()
        {
            var studentRepo = new Mock<IStudentRepository>();
            var service = ProduceStudentService(studentRepo);

            var result = service.CreateStudent(ProduceInvalidStudent());

            Assert.Null(result.student);
            Assert.NotNull(result.error);
            Assert.NotNull(result.error.AggregatedErrors);
            Assert.True(result.error.AggregatedErrors.Any());
            Assert.Equal(ErrorIds.Domain.InputValidationError, result.error.ErrorId);
        }

        [Fact]
        public void StudentService_CreateStudent_Invalid_Student_Single_Validation_Error()
        {
            var studentRepo = new Mock<IStudentRepository>();
            var service = ProduceStudentService(studentRepo);

            var result = service.CreateStudent(ProduceSlightlyInvalidStudent());

            Assert.Null(result.student);
            Assert.NotNull(result.error);
            Assert.Equal(ErrorIds.Domain.InputValidationError, result.error.ErrorId);
        }

        [Fact]
        public void StudentService_CreateStudent_Success()
        {
            var studentRepoMock = new Mock<IStudentRepository>();

            studentRepoMock.Setup(repo => repo.CreateStudent(It.IsAny<StudentEntity>())).Returns((StudentEntity student) =>
            {
                student.Id = 1;
                return (student,null);
            });

            var service = ProduceStudentService(studentRepoMock);
            var validStudent = ProduceValidStudent();
            var result = service.CreateStudent(validStudent);

            Assert.Null(result.error);
            Assert.NotNull(result.student);
            Assert.NotEqual(0, result.student.Id);
        }

        [Fact]
        public void StudentService_CreateStudent_Repository_Failure()
        {
            var studentRepoMock = new Mock<IStudentRepository>();
            studentRepoMock.Setup(repo => repo.CreateStudent(It.IsAny<StudentEntity>())).Returns((StudentEntity student) => (null, new Error("Database Error",ErrorIds.Data.UnknownError)));

            var service = ProduceStudentService(studentRepoMock);

            var validStudent = ProduceValidStudent();
            var result = service.CreateStudent(validStudent);

            Assert.Null(result.student);
            Assert.NotNull(result.error);
            Assert.Equal(ErrorIds.Data.UnknownError, result.error.ErrorId);
        }
    }
}