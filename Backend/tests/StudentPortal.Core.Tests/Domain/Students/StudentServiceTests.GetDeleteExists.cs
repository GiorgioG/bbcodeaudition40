﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using StudentPortal.Core.Contract.Data.Repository;
using StudentPortal.Core.Data.Models;
using Xunit;

namespace StudentPortal.Core.Tests.Domain.Students
{
    public partial class StudentServiceTests
    {
        [Fact]
        public void StudentService_Get_Student_By_Id_RepositorySuccess()
        {
            var studentRepoMock = new Mock<IStudentRepository>();
            studentRepoMock.Setup(repo => repo.GetStudentById(It.IsAny<int>())).Returns(() => (ProduceValidStudent(), null));

            var service = ProduceStudentService(studentRepoMock);
            var result = service.GetStudentById(0);

            Assert.NotNull(result);
            Assert.NotNull(result.student);
            Assert.Null(result.error);
        }

        [Fact]
        public void StudentService_Get_Students_RepositorySuccess()
        {
            var studentRepoMock = new Mock<IStudentRepository>();
            studentRepoMock.Setup(repo => repo.GetStudents(It.IsAny<int>(), It.IsAny<int>())).Returns(() => new List<StudentEntity> { ProduceValidStudent(), ProduceValidStudent() });

            var service = ProduceStudentService(studentRepoMock);
            var students = service.GetStudents();

            Assert.NotNull(students);
            Assert.True(students.Any());
        }
        [Fact]
        public void StudentService_DeleteStudent_Success()
        {
            var studentRepoMock = new Mock<IStudentRepository>();
            studentRepoMock.Setup(repo => repo.DeleteStudent(It.IsAny<int>())).Returns(() => null);

            var service = ProduceStudentService(studentRepoMock);

            var error = service.DeleteStudent(0);
            Assert.Null(error);
        }

        [Fact]
        public void StudentService_Student_Exists_Success()
        {
            var studentRepoMock = new Mock<IStudentRepository>();
            studentRepoMock.Setup(repo => repo.Exists(It.IsAny<int>())).Returns(() => true);

            var service = ProduceStudentService(studentRepoMock);
            var exists = service.Exists(0);

            Assert.True(exists);
        }
    }
}
