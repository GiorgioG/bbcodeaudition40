﻿using System;
using System.Linq;
using Moq;
using StudentPortal.Core.Contract.Data.Repository;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Shared.Errors;
using Xunit;

namespace StudentPortal.Core.Tests.Domain.Students
{
    /// <summary>
    /// Class StudentService.
    /// </summary>
    public sealed partial class StudentServiceTests
    {
        [Fact]
        public void StudentService_UpdateStudent_NullStudent()
        {
            var studentRepo = new Mock<IStudentRepository>();
            var service = ProduceStudentService(studentRepo);

            Assert.Throws<ArgumentNullException>(()=>service.UpdateStudent(null));
        }

        [Fact]
        public void StudentService_UpdateStudent_InvalidStudent_MultipleValidationErrors()
        {
            var studentRepo = new Mock<IStudentRepository>();
            var service = ProduceStudentService(studentRepo);
            var result = service.UpdateStudent(ProduceInvalidStudent());

            Assert.Null(result.student);
            Assert.NotNull(result.error);
            Assert.NotNull(result.error.AggregatedErrors);
            Assert.True(result.error.AggregatedErrors.Any());
            Assert.Equal(ErrorIds.Domain.InputValidationError, result.error.ErrorId);
        }

        [Fact]
        public void StudentService_UpdateStudent_InvalidStudent_SingleValidationError()
        {
            var studentRepo = new Mock<IStudentRepository>();
            var service = ProduceStudentService(studentRepo);
            var result = service.UpdateStudent(ProduceSlightlyInvalidStudent());

            Assert.Null(result.student);
            Assert.NotNull(result.error);
            Assert.Equal(ErrorIds.Domain.InputValidationError, result.error.ErrorId);
        }

        [Fact]
        public void StudentService_UpdateStudent_Success()
        {
            var studentRepoMock = new Mock<IStudentRepository>();
            var validStudent = ProduceValidStudent();

            studentRepoMock.Setup(repo => repo.UpdateStudent(It.IsAny<StudentEntity>())).Returns((StudentEntity student) =>
            {
                student.Id = 1;
                return (student,null);
            });

            var service = ProduceStudentService(studentRepoMock);
            var result = service.UpdateStudent(validStudent);

            Assert.Null(result.error);
            Assert.NotNull(result.student);
            Assert.NotEqual(0, result.student.Id);
        }

        [Fact]
        public void StudentService_UpdateStudent_RepositoryFailure()
        {
            var studentRepoMock = new Mock<IStudentRepository>();
            var validStudent = ProduceValidStudent();

            studentRepoMock.Setup(repo => repo.UpdateStudent(It.IsAny<StudentEntity>())).Returns(() => (null, new Error("Database Error",ErrorIds.Data.UnknownError)));

            var service = ProduceStudentService(studentRepoMock);
            var result = service.UpdateStudent(validStudent);

            Assert.Null(result.student);
            Assert.NotNull(result.error);
            Assert.Equal(ErrorIds.Data.UnknownError, result.error.ErrorId);
        }
    }
}