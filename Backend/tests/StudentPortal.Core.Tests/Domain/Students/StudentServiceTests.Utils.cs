﻿using System;
using Moq;
using StudentPortal.Core.Contract.Data.Repository;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Domain.Students;
using StudentPortal.Core.Domain.Students.Validators;

namespace StudentPortal.Core.Tests.Domain.Students
{
    public partial class StudentServiceTests
    {

        /// <summary>
        /// An invalid student with several validation issues
        /// </summary>
        /// <returns></returns>
        private StudentEntity ProduceInvalidStudent()
        {
            return new StudentEntity()
            {
                DateOfBirth = DateTime.UtcNow.AddYears(-151),
                Grade = "100"
            };
        }

        private StudentEntity ProduceValidStudent()
        {
            return new StudentEntity()
            {
                FirstName = "Gabby",
                LastName = "Galante",
                DateOfBirth = DateTime.UtcNow.AddYears(-10),
                Grade = "6"
            };
        }
        /// <summary>
        /// An invalid student with no first name
        /// </summary>
        /// <returns></returns>
        private StudentEntity ProduceSlightlyInvalidStudent()
        {
            var student = ProduceValidStudent();
            student.FirstName = null;
            return student;
        }

        private static StudentService ProduceStudentService(Mock<IStudentRepository> studentRepo)
        {
            StudentValidator StudentValidatorFactory() => new StudentValidator();

            var service = new StudentService(studentRepo.Object, StudentValidatorFactory);
            return service;
        }
    }
}
