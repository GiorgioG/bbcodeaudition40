﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using StudentPortal.Core.Contract.Data.Repository;
using StudentPortal.Core.Contract.Domain.Students;
using StudentPortal.Core.Contract.Presentation.Converters;
using StudentPortal.Core.Data.Repository;
using StudentPortal.Core.Domain.Students;
using StudentPortal.Core.Domain.Students.Validators;
using StudentPortal.Core.Presentation.Converters;

namespace StudentPortal.API
{
    public partial class Startup
    {

        private void ConfigureApplicationContainer(IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.Populate(services);
            RegisterDataLayerTypes(builder);
            RegisterDomainLayerTypes(builder);
            RegisterPresentationLayerTypes(builder);
            ApplicationContainer = builder.Build();
        }

        private void RegisterPresentationLayerTypes(ContainerBuilder builder)
        {
            builder.RegisterType<StudentConverter>().As<IStudentConverter>().InstancePerLifetimeScope();
        }

        private void RegisterDomainLayerTypes(ContainerBuilder builder)
        {
            builder.RegisterType<StudentService>().As<IStudentService>();
            builder.RegisterType<StudentValidator>();
        }

        private void RegisterDataLayerTypes(ContainerBuilder builder)
        {
            builder.Register(c =>
            {
                var configurationSection = Configuration.GetSection("Database:ConnectionString");
                var connectionString = configurationSection.Value;

                return new RepositoryConfiguration(connectionString);
            }).As<RepositoryConfiguration>();

            builder.RegisterType<StudentRepository>().As<IStudentRepository>().InstancePerLifetimeScope();
        }
    }
}