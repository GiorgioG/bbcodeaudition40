﻿using Microsoft.AspNetCore.Mvc;
using StudentPortal.Core.Contract.Domain.Students;
using StudentPortal.Core.Contract.Presentation.Converters;
using StudentPortal.Core.Presentation.Models;
using System;
using System.Linq;

namespace StudentPortal.API.Controllers
{
    /// <summary>
    /// Class StudentController. This class cannot be inherited.
    /// </summary>
    /// <seealso cref="Microsoft.AspNetCore.Mvc.Controller" />
    [Route("api/[controller]")]
    public sealed class StudentController : Controller
    {
        /// <summary>
        /// The student service
        /// </summary>
        private readonly IStudentService studentService;
        /// <summary>
        /// The student converter
        /// </summary>
        private readonly IStudentConverter studentConverter;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentController"/> class.
        /// </summary>
        /// <param name="studentService">The student service.</param>
        /// <param name="studentConverter">The student converter.</param>
        /// <exception cref="System.ArgumentNullException">
        /// studentService
        /// or
        /// studentConverter
        /// </exception>
        public StudentController(IStudentService studentService, IStudentConverter studentConverter)
        {
            this.studentService = studentService ?? throw new ArgumentNullException(nameof(studentService));
            this.studentConverter = studentConverter ?? throw new ArgumentNullException(nameof(studentConverter));
        }
        /// <summary>
        /// Gets all student records.
        /// TODO: Expose pagination support
        /// </summary>
        /// <returns>IActionResult.</returns>
        [HttpGet]
        public IActionResult Get()
        {
            var studentEntities = studentService.GetStudents();
            var studentPresentationModels = studentEntities.Select(studentConverter.ToPresentationModel);
            return Ok(studentPresentationModels);
        }

        /// <summary>
        /// Gets the specified studenty by Id.
        /// </summary>
        /// <param name="id">The student's identifier.</param>
        /// <returns>IActionResult.</returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            if (id <= 0) return BadRequest("Invalid id");

            if (!studentService.Exists(id))
                return NotFound();

            var result = studentService.GetStudentById(id);

            if (result.error == null)
                return Ok(studentConverter.ToPresentationModel(result.student));
            else
                return BadRequest(result.error);
        }

        // POST api/values
        /// <summary>
        /// Posts the specified student model.
        /// </summary>
        /// <param name="studentPresentationModel">The student model.</param>
        /// <returns>IActionResult.</returns>
        [HttpPost]
        public IActionResult Post([FromBody] StudentPresentationModel studentPresentationModel)
        {
            if (studentPresentationModel == null) return BadRequest("Invalid/missing request body.");

            var studentEntity = studentConverter.ToEntity(studentPresentationModel);
            var result = studentService.CreateStudent(studentEntity);
            if (result.error == null)
            {
                var uri = new Uri(Url.Content("~/api/student/"+result.student.Id),UriKind.Relative);
                return Created(uri, studentConverter.ToPresentationModel(result.student));
            }
            else
            {
                return BadRequest(result.error);
            }
        }

        // PUT api/values/5
        /// <summary>
        /// Updates the specified student.
        /// </summary>
        /// <param name="id">The student identifier.</param>
        /// <param name="studentPresentationModel">The student model.</param>
        /// <returns>IActionResult.</returns>
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] StudentPresentationModel studentPresentationModel)
        {
            if (id <= 0) return BadRequest("Invalid id");
            if (studentPresentationModel == null) return BadRequest("Invalid/missing request body.");
            if (id != studentPresentationModel.Id) return BadRequest("Uri/model id mismatch.");

            if (!studentService.Exists(id))
                return NotFound();

            var studentEntity = studentConverter.ToEntity(studentPresentationModel);

            var result = studentService.UpdateStudent(studentEntity);
            if (result.error == null)
                return Accepted();
            else
                return BadRequest(result.error);
        }

        /// <summary>
        /// Deletes the specified student.
        /// </summary>
        /// <param name="id">The student identifier.</param>
        /// <returns>IActionResult.</returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id <= 0) return BadRequest("Invalid id");
            if (!studentService.Exists(id))
                return NotFound();

            var error = studentService.DeleteStudent(id);
            if (error == null)
                return Accepted();
            else
                return BadRequest(error);
        }
    }
}
