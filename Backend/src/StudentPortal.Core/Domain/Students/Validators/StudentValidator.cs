﻿using System;
using FluentValidation;
using StudentPortal.Core.Data.Models;

namespace StudentPortal.Core.Domain.Students.Validators
{
    /// <summary>
    /// Class StudentValidator.
    /// </summary>
    /// <seealso cref="FluentValidation.AbstractValidator{StudentEntity}" />
    public sealed class StudentValidator : AbstractValidator<StudentEntity>
    {
        /// <summary>
        /// The maximum supported/valid age
        /// </summary>
        private const int MaxAge = 150;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentValidator"/> class.
        /// </summary>
        public StudentValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().MinimumLength(1).MaximumLength(100);
            RuleFor(x => x.LastName).NotEmpty().MinimumLength(1).MaximumLength(100);
            RuleFor(x => x.Grade).NotEmpty().MinimumLength(1).MaximumLength(2);
            RuleFor(x => x.DateOfBirth).Must(x => x.Year >= DateTime.UtcNow.Year - MaxAge).WithMessage($"DateOfBirth must be within the last ${MaxAge} years.");
            RuleFor(x => x.DateOfBirth).Must(x => x.Date < DateTime.Today).WithMessage($"DateOfBirth cannot be a future date.");
        }
    }
}