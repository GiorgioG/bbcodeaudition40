﻿using System;
using System.Collections.Generic;
using System.Linq;
using StudentPortal.Core.Contract.Data.Repository;
using StudentPortal.Core.Contract.Domain.Students;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Domain.Students.Validators;
using StudentPortal.Core.Shared.Errors;

namespace StudentPortal.Core.Domain.Students
{
    /// <summary>
    /// Class StudentService.
    /// </summary>
    public class StudentService : IStudentService
    {
        /// <summary>
        /// The student repository
        /// </summary>
        private readonly IStudentRepository studentRepository;

        /// <summary>
        /// The student validator factory
        /// </summary>
        private readonly Func<StudentValidator> studentValidatorFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentService" /> class.
        /// </summary>
        /// <param name="studentRepository">The student repository.</param>
        /// <param name="studentValidatorFactory">The student validator factory.</param>
        /// <exception cref="System.ArgumentNullException">studentValidatorFactory
        /// or
        /// studentRepositoryFactory</exception>
        public StudentService(IStudentRepository studentRepository, Func<StudentValidator> studentValidatorFactory)
        {
            this.studentValidatorFactory = studentValidatorFactory ?? throw new ArgumentNullException(nameof(studentValidatorFactory));
            this.studentRepository = studentRepository ?? throw new ArgumentNullException(nameof(studentRepository));
        }

        /// <summary>
        /// Creates the student.
        /// </summary>
        /// <param name="student">The student.</param>
        /// <returns>Tuple of (int, Error).</returns>
        /// <exception cref="System.ArgumentNullException">student</exception>
        public (StudentEntity student, Error error) CreateStudent(StudentEntity student)
        {
            if (student == null) throw new ArgumentNullException(nameof(student));

            var validator = studentValidatorFactory();
            var validationResult = validator.Validate(student);

            if (validationResult.IsValid)
            {
                var result = studentRepository.CreateStudent(student);
                if (result.error == null)
                    return (result.student, null);
                else
                    return result;
            }
            else
            {
                var failures = validationResult.Errors;
                if (failures.Count == 1)
                    return (null, failures.First().ToValidationError());
                else
                    return (null, new Error("Multiple Validation Failures", ErrorIds.Domain.InputValidationError, failures.ToValidationErrors()));
            }
        }

        /// <summary>
        /// Updates the student.
        /// </summary>
        /// <param name="student">The student.</param>
        /// <returns>Error.</returns>
        /// <exception cref="System.ArgumentNullException">student</exception>
        public (StudentEntity student, Error error) UpdateStudent(StudentEntity student)
        {
            if (student == null) throw new ArgumentNullException(nameof(student));

            var validator = studentValidatorFactory();
            var validationResult = validator.Validate(student);

            if (validationResult.IsValid)
                return studentRepository.UpdateStudent(student);

            var failures = validationResult.Errors;
            if (failures.Count == 1)
                return (null, failures.First().ToValidationError());

            return (null, new Error("Multiple Validation Failures", ErrorIds.Domain.InputValidationError, failures.ToValidationErrors()));
        }

        public Error DeleteStudent(int id)
        {
            return studentRepository.DeleteStudent(id);
        }

        /// <summary>
        /// Student exists record check
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns><c>true</c> if student exists, <c>false</c> otherwise.</returns>
        public bool Exists(int id)
        {
            return studentRepository.Exists(id);
        }

        /// <summary>
        /// Gets the student by Id.
        /// </summary>
        /// <param name="id">The student Id.</param>
        /// <returns>Tuple of StudentEntity, Error</returns>
        public (StudentEntity student, Error error) GetStudentById(int id)
        {
            return studentRepository.GetStudentById(id);
        }

        /// <summary>
        /// Gets a paginated list of students.
        /// </summary>
        /// <param name="pageSize">the page size.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <returns>IEnumerable&lt;StudentEntity&gt;.</returns>
        public IEnumerable<StudentEntity> GetStudents(int pageSize = 20, int pageNumber = 1)
        {
            return studentRepository.GetStudents(pageSize, pageNumber);
        }
    }
}