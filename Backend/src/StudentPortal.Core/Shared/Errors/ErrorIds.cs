﻿namespace StudentPortal.Core.Shared.Errors
{
    /// <summary>
    /// Class ErrorIds.
    /// </summary>
    public static class ErrorIds
    {
        /// <summary>
        /// Class Data.
        /// </summary>
        public static class Data
        {
            /// <summary>
            /// Represents an unknown error
            /// </summary>
            public static readonly int UnknownError = 1;
            /// <summary>
            /// Represents a not found error
            /// </summary>
            public static readonly int NotFound = 2;
        }

        /// <summary>
        /// Class Domain.
        /// </summary>
        public static class Domain
        {
            /// <summary>
            /// Represents an input validation error
            /// </summary>
            public static readonly int InputValidationError = 10000;
        }

        /// <summary>
        /// Class Presentation.
        /// </summary>
        public static class Presentation
        {
            /// <summary>
            /// Represents a null payload
            /// </summary>
            public static readonly int NullPayload = 20000;
            /// <summary>
            /// Represents an invalid date of birth
            /// </summary>
            public static readonly int InvalidDateOfBirth = 20001;
        }
    }
}