﻿using System.Collections.Generic;

namespace StudentPortal.Core.Shared.Errors
{
    /// <summary>
    /// Class Error. This class cannot be inherited.
    /// </summary>
    public sealed class Error
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Error"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="errorId">The error identifier.</param>
        /// <param name="aggregatedErrors">The aggregated errors.</param>
        public Error(string message, int errorId, IEnumerable<Error> aggregatedErrors = null)
        {
            Message = message;
            ErrorId = errorId;
            AggregatedErrors = aggregatedErrors ?? new Error[0];
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get; }
        /// <summary>
        /// Gets the error identifier.
        /// </summary>
        /// <value>The error identifier.</value>
        public int ErrorId { get; }
        /// <summary>
        /// Gets the aggregated errors.
        /// </summary>
        /// <value>The aggregated errors.</value>
        public IEnumerable<Error> AggregatedErrors { get; }
    }
}