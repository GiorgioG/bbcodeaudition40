﻿using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace StudentPortal.Core.Shared.Errors
{
    /// <summary>
    /// Class ValidationErrorExtensions.
    /// </summary>
    public static class ValidationErrorExtensions
    {
        /// <summary>
        /// Converts a validation failure to Error.
        /// </summary>
        /// <param name="failure">The failure.</param>
        /// <returns>Error.</returns>
        public static Error ToValidationError(this ValidationFailure failure)
        {
            return new Error(failure.ErrorMessage, ErrorIds.Domain.InputValidationError);
        }

        /// <summary>
        /// Converts validation failures to a list of errors.
        /// </summary>
        /// <param name="failures">The failures.</param>
        /// <returns>IEnumerable&lt;Error&gt;.</returns>
        public static IEnumerable<Error> ToValidationErrors(this IList<ValidationFailure> failures)
        {
            return failures?.Select(ToValidationError);
        }
    }
}