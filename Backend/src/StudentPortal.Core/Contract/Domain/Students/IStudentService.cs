﻿using System.Collections.Generic;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Shared.Errors;

namespace StudentPortal.Core.Contract.Domain.Students
{
    /// <summary>
    /// Interface IStudentService
    /// </summary>
    public interface IStudentService
    {
        /// <summary>
        /// Creates the student.
        /// </summary>
        /// <param name="student">The student.</param>
        /// <returns>System.ValueTuple.StudentEntity.Error.</returns>
        (StudentEntity student, Error error) CreateStudent(StudentEntity student);
        /// <summary>
        /// Existses the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        bool Exists(int id);
        /// <summary>
        /// Gets the student by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>System.ValueTuple.StudentEntity.Error.</returns>
        (StudentEntity student, Error error) GetStudentById(int id);
        /// <summary>
        /// Gets the students.
        /// </summary>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <returns>IEnumerable&lt;StudentEntity&gt;.</returns>
        IEnumerable<StudentEntity> GetStudents(int pageSize = 100, int pageNumber = 1);
        /// <summary>
        /// Updates the student.
        /// </summary>
        /// <param name="student">The student.</param>
        /// <returns>System.ValueTuple.StudentEntity.Error.</returns>
        (StudentEntity student, Error error) UpdateStudent(StudentEntity student);
        /// <summary>
        /// Deletes the student.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Error.</returns>
        Error DeleteStudent(int id);
    }
}