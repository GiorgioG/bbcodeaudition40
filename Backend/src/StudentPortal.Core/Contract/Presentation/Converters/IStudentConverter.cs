﻿using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Presentation.Models;

namespace StudentPortal.Core.Contract.Presentation.Converters
{
    /// <summary>
    /// Interface IStudentConverter
    /// </summary>
    public interface IStudentConverter
    {
        /// <summary>
        /// To the entity.
        /// </summary>
        /// <param name="presentationModel">The presentation model.</param>
        /// <returns>StudentEntity.</returns>
        StudentEntity ToEntity(StudentPresentationModel presentationModel);
        /// <summary>
        /// To the presentation model.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>StudentPresentationModel.</returns>
        StudentPresentationModel ToPresentationModel(StudentEntity entity);
    }
}