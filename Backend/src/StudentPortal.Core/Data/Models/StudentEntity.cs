﻿using System;

namespace StudentPortal.Core.Data.Models
{
    /// <summary>
    /// Class StudentEntity. This class cannot be inherited.
    /// </summary>
    /// <seealso cref="StudentPortal.Core.Data.Models.Entity" />
    public sealed class StudentEntity : Entity
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName { get; set; }
        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName { get; set; }
        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        /// <value>The date of birth.</value>
        public DateTime DateOfBirth { get; set; }
        /// <summary>
        /// Gets or sets the grade.
        /// </summary>
        /// <value>The grade.</value>
        public string Grade { get; set; }
    }
}