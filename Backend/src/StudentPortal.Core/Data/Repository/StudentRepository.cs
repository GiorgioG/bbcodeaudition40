﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using StudentPortal.Core.Contract.Data.Repository;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Shared.Errors;

namespace StudentPortal.Core.Data.Repository
{
    /// <summary>
    /// Class StudentRepository.
    /// </summary>
    /// <seealso cref="StudentPortal.Core.Contract.Data.Repository.IStudentRepository" />
    /// <seealso cref="StudentPortal.Core.Data.Repository.Repository" />
    public sealed class StudentRepository : Repository, IStudentRepository
    {
        /// <summary>
        /// The base student query
        /// </summary>
        private const string BaseStudentQuery = @"SELECT [Id]
                                                        ,[FirstName]
                                                        ,[LastName]
                                                        ,[Grade]
                                                        ,[DOB] AS DateOfBirth
                                                        ,[DateCreated]
                                                        ,[DateLastModified]
                                                  FROM [Student]";

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentRepository" /> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        public StudentRepository(RepositoryConfiguration configuration) : base(configuration)
        {
        }

        /// <summary>
        /// Creates the student.
        /// </summary>
        /// <param name="student">The student.</param>
        /// <returns>System.ValueTuple.StudentEntity.Error.</returns>
        public (StudentEntity student, Error error) CreateStudent(StudentEntity student)
        {
            const string sql = @"INSERT INTO [Student]
                                    ([FirstName]
                                    ,[LastName]
                                    ,[Grade]
                                    ,[DOB]
                                    ,[DateCreated])
                                VALUES
                                    (@FirstName 
                                    ,@LastName
                                    ,@Grade
                                    ,@DateOfBirth
                                    ,@DateCreated)
                                SELECT SCOPE_IDENTITY()";

            using (var conn = GetConnection())
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("@FirstName", student.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", student.LastName);
                    cmd.Parameters.AddWithValue("@Grade", student.Grade);
                    cmd.Parameters.AddWithValue("@DateOfBirth", student.DateOfBirth);
                    cmd.Parameters.AddWithValue("@DateCreated", DateTime.UtcNow);
                    conn.Open();
                    var id = Convert.ToInt32(cmd.ExecuteScalar());
                    conn.Close();
                    return GetStudentById(id);
                }
            }
        }

        /// <summary>
        /// Updates the student.
        /// </summary>
        /// <param name="student">The student.</param>
        /// <returns>Error.</returns>
        public (StudentEntity student, Error error) UpdateStudent(StudentEntity student)
        {
            if (!Exists(student.Id)) return (null, new Error("Unable to update student, record does not exist.", ErrorIds.Data.NotFound));

            const string sql = @"UPDATE [Student]
                                SET [FirstName] = @FirstName
                                    ,[LastName] = @LastName
                                    ,[Grade] = @Grade
                                    ,[DOB] = @DateOfBirth
                                    ,[DateLastModified] = @DateLastModified
                                WHERE Id=@Id AND EntityState=0";

            using (var conn = GetConnection())
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("@FirstName", student.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", student.LastName);
                    cmd.Parameters.AddWithValue("@Grade", student.Grade);
                    cmd.Parameters.AddWithValue("@DateOfBirth", student.DateOfBirth);
                    cmd.Parameters.AddWithValue("@DateLastModified", DateTime.UtcNow);
                    cmd.Parameters.AddWithValue("@Id", student.Id);

                    conn.Open();
                    var rowsAffected = cmd.ExecuteNonQuery();
                    conn.Close();

                    if (rowsAffected == 0) return (null, new Error("Unable to update student, reason unknown", ErrorIds.Data.UnknownError));
                }
            }

            return GetStudentById(student.Id);
        }

        /// <summary>
        /// Deletes the student.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Error.</returns>
        public Error DeleteStudent(int id)
        {
            if (!Exists(id)) return new Error("Unable to update student, record does not exist.", ErrorIds.Data.NotFound);

            const string sql = @"UPDATE [Student]
                                SET [EntityState] = -1
                                WHERE Id=@Id";

            using (var conn = GetConnection())
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("@Id", id);

                    conn.Open();
                    var rowsAffected = cmd.ExecuteNonQuery();
                    conn.Close();

                    if (rowsAffected == 0) return new Error("Unable to delete student, reason unknown", ErrorIds.Data.UnknownError);
                }
            }

            return null;
        }

        /// <summary>
        /// Student exists check
        /// </summary>
        /// <param name="id">The student id.</param>
        /// <returns><c>true</c> if the student exists, <c>false</c> otherwise.</returns>
        public bool Exists(int id)
        {
            const string sql = @"SELECT 
                                    CAST(CASE WHEN EXISTS(
                                        SELECT * FROM [Student] 
                                        WHERE Id = @Id AND [EntityState]=0) 
                                            THEN 1 
                                            ELSE 0 END
                                    AS BIT)";

            using (var conn = GetConnection())
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("@Id", id);
                    conn.Open();
                    return Convert.ToBoolean(cmd.ExecuteScalar());
                }
            }
        }

        /// <summary>
        /// Gets the student by their database Id.
        /// </summary>
        /// <param name="id">The student Id.</param>
        /// <returns>Tuple of StudentEntity, Error</returns>
        public (StudentEntity student, Error error) GetStudentById(int id)
        {
            var sql = $@"{BaseStudentQuery}
                                 WHERE Id=@Id AND EntityState=0";

            using (var conn = GetConnection())
            {
                conn.Open();
                var entity = conn.QueryFirstOrDefault<StudentEntity>(sql, new {Id = id});
                conn.Close();

                if (entity == null) return (null, new Error("Student Not Found", ErrorIds.Data.NotFound));

                return (entity, null);
            }
        }

        /// <summary>
        /// Gets a paginated list of students ordered by Id
        /// </summary>
        /// <param name="pageSize">The page size.</param>
        /// <param name="pageNumber">The page number.</param>
        /// <returns>IEnumerable&lt;StudentEntity&gt;.</returns>
        public IEnumerable<StudentEntity> GetStudents(int pageSize = 20, int pageNumber = 1)
        {
            var sql = $@"{BaseStudentQuery}
                                  WHERE EntityState=0
                                  ORDER BY Id
                                  OFFSET @PageSize*(@PageNumber-1) ROWS
                                  FETCH NEXT @PageSize ROWS ONLY";

            using (var conn = GetConnection())
            {
                conn.Open();
                return conn.Query<StudentEntity>(sql, new {PageSize = pageSize, PageNumber = pageNumber});
            }
        }
    }
}