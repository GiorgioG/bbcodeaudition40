﻿using System;
using System.Data.SqlClient;

namespace StudentPortal.Core.Data.Repository
{
    /// <summary>
    /// Class Repository.
    /// </summary>
    public abstract class Repository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <exception cref="ArgumentNullException">configuration</exception>
        protected Repository(RepositoryConfiguration configuration)
        {
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        /// <summary>
        /// Gets the configuration.
        /// </summary>
        /// <value>The configuration.</value>
        protected RepositoryConfiguration Configuration { get; }

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <returns>SqlConnection.</returns>
        protected SqlConnection GetConnection()
        {
            return new SqlConnection(Configuration.ConnectionString);
        }
    }
}