﻿using System;

namespace StudentPortal.Core.Data.Repository
{
    /// <summary>
    /// Class RepositoryConfiguration.
    /// </summary>
    public class RepositoryConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RepositoryConfiguration"/> class.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">connectionString - cannot be null or empty</exception>
        public RepositoryConfiguration(string connectionString)
        {
            ConnectionString = string.IsNullOrWhiteSpace(connectionString) ? throw new ArgumentOutOfRangeException(nameof(connectionString), "cannot be null or empty") : connectionString;
        }

        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public string ConnectionString { get; }
    }
}