﻿using StudentPortal.Core.Contract.Presentation.Converters;
using StudentPortal.Core.Data.Models;
using StudentPortal.Core.Presentation.Models;

namespace StudentPortal.Core.Presentation.Converters
{
    /// <summary>
    /// Class StudentConverter.
    /// In a more sophisticated / complicated implementation I'd use Automapper for this
    /// </summary>
    public class StudentConverter : IStudentConverter
    {
        /// <summary>
        /// Converts presentation model to its entity representation.
        /// </summary>
        /// <param name="presentationModel">The presentation model.</param>
        /// <returns>StudentEntity.</returns>
        public StudentEntity ToEntity(StudentPresentationModel presentationModel)
        {
            if (presentationModel == null) return null;

            var entity = new StudentEntity
            {
                Id = presentationModel.Id,
                FirstName = presentationModel.FirstName,
                LastName = presentationModel.LastName,
                DateOfBirth = presentationModel.DateOfBirth,
                Grade = presentationModel.Grade,
                DateCreated = presentationModel.DateCreated,
                DateLastModified = presentationModel.DateLastModified
            };

            return entity;
        }

        /// <summary>
        /// Converts the entity/domain model to the presentation model.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <returns>StudentPresentationModel.</returns>
        public StudentPresentationModel ToPresentationModel(StudentEntity entity)
        {
            if (entity == null) return null;

            var presentationModel = new StudentPresentationModel
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                DateOfBirth = entity.DateOfBirth,
                Grade = entity.Grade,
                DateCreated = entity.DateCreated,
                DateLastModified = entity.DateLastModified
            };

            return presentationModel;
        }
    }
}