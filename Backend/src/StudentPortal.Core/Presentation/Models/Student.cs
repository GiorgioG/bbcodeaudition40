﻿using System;

namespace StudentPortal.Core.Presentation.Models
{
    /// <summary>
    /// Class StudentPresentationModel. This class cannot be inherited.
    /// </summary>
    /// <seealso cref="StudentPortal.Core.Presentation.Models.Model" />
    public sealed class StudentPresentationModel : Model
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName { get; set; }
        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName { get; set; }
        /// <summary>
        /// Gets or sets the date of birth.
        /// </summary>
        /// <value>The date of birth.</value>
        public DateTime DateOfBirth { get; set; }
        /// <summary>
        /// Gets or sets the grade.
        /// </summary>
        /// <value>The grade.</value>
        public string Grade { get; set; }
    }
}